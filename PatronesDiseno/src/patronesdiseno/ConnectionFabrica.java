/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno;

import patronesdiseno.fabrica.IConnectionFabrica;
import patronesdiseno.fabrica.implement.ConnectionMySQL;
import patronesdiseno.fabrica.implement.ConnectionOracle;
import patronesdiseno.fabrica.implement.ConnectionSQL;
import patronesdiseno.fabrica.implement.ConnectionVacia;

/**
 *
 * @author edgar
 */
public class ConnectionFabrica {
    
    public IConnectionFabrica getConexion(String motor){
        if (motor == null){
           return new ConnectionVacia();            
        }
        if (motor.equalsIgnoreCase("MYSQL")){
            return new ConnectionMySQL();
        } else if(motor.equalsIgnoreCase("ORACLE")){
            return new ConnectionOracle();
        } else if (motor.equalsIgnoreCase("SQL")){
            return new ConnectionSQL();
        }
        return new ConnectionVacia();
        
    }
}
