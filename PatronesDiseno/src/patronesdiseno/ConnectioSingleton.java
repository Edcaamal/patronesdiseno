/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno;

/**
 *
 * @author edgar
 */
public class ConnectioSingleton {
    //Declaración de variable instancia de tipo ConnextionSingleton
    private static ConnectioSingleton instancia;
    // private static ConnectioSingleton intancia = new ConnectioSingleton();
    
    // Creamos constructor instancia para evitar se cree meninte operador "new" multiples instancia
    private ConnectioSingleton(){
        
    }
    
    // para obtener la instancia unicamente por este métodp
    // la palabra reservada static hace posible el acceso mediante Clase.metodo
    // generarlo meiante el asistente
    public static ConnectioSingleton getInstancia() {
        if (instancia == null){
            instancia = new ConnectioSingleton();
        }
        return instancia;
    }
    
    // Métodos de prueba  
    public void conectar(){
        System.out.println("Conexión exitosa a la D.B.");
    }
    
    public void desconectar(){
        System.out.println("Cerrarndo conexión con  la D.B.");
    }
    
}
