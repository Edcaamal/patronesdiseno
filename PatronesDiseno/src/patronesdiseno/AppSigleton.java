/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno;

/**
 *
 * @author edgar
 */
public class AppSigleton {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO: code application logic here
        System.out.println("1.- Patron de Diseno: Singleton");
        System.out.println("------------------------------------------");
        // Instanciación por constructor esta prohibido por ser "private"
        // ConnectioSingleton C = new ConnectioSingleton();
        ConnectioSingleton connDB = ConnectioSingleton.getInstancia();
        connDB.conectar();
        connDB.desconectar();
        
        // Comprobando que connDB es una instancia de ConnectioSingleton
        boolean rpta = connDB instanceof ConnectioSingleton;
        System.out.println(rpta);
    }
    
}
