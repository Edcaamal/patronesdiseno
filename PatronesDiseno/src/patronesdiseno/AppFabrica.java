/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno;

import patronesdiseno.fabrica.IConnectionFabrica;

/**
 *
 * @author edgar
 */
public class AppFabrica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("2.- Patron de Diseno: Fabrica");
        System.out.println("------------------------------------------");
        
        ConnectionFabrica fabrica = new ConnectionFabrica();
        
        IConnectionFabrica cx1 = fabrica.getConexion("MYSQL");
        cx1.conectar ();
        cx1.desconectar();

        IConnectionFabrica cx2 = fabrica.getConexion("ORACLE");
        cx2.conectar ();
        cx2.desconectar();

        IConnectionFabrica cx3 = fabrica.getConexion("SQL");
        cx3.conectar ();
        cx3.desconectar();

        IConnectionFabrica cx4 = fabrica.getConexion("NOSQL");
        cx4.conectar ();
        cx4.desconectar();
        
    }
    
}
