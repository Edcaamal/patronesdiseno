/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno.fabrica.implement;

import patronesdiseno.fabrica.IConnectionFabrica;

/**
 *
 * @author edgar
 */
public class ConnectionSQL implements IConnectionFabrica{
    
    private String host;
    private String puerto;
    private String usuario;
    private String pass;

    public ConnectionSQL() {
        this.host    = "localhost";
        this.puerto  = "1521";
        this.usuario = "admin";
        this.pass    = "123 ";
    }
    
    @Override
    public void conectar() {
        System.out.println("Conexión a SQL exitosa");
     }

    @Override
    public void desconectar() {
        System.out.println("Cerrando conexión a SQL");
     }

    @Override
    public String toString() {
        return "ConnectionSQL{" + "host=" + host + ", puerto=" + puerto + ", usuario=" + usuario + ", pass=" + pass + '}';
    }
   
    
}
