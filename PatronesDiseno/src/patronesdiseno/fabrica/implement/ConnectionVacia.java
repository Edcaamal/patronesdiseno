/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno.fabrica.implement;

import patronesdiseno.fabrica.IConnectionFabrica;

/**
 *
 * @author edgar
 */
public class ConnectionVacia implements IConnectionFabrica {
    
    @Override
    public void conectar(){
        System.out.println("No se especificó el proveedor de Conexión a DB");
    }
    
    @Override
    public void desconectar(){
        System.out.println("No se especificó el proveedor de Conexión a DB");
    }

}
