/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno.fabrica.implement;

import patronesdiseno.fabrica.IConnectionFabrica;

/**
 *
 * @author edgar
 */
public class ConnectionMySQL implements IConnectionFabrica{
    
    private String host;
    private String puerto;
    private String usuario;
    private String pass;

    public ConnectionMySQL() {
        this.host    = "localhost";
        this.puerto  = "3306";
        this.usuario = "root";
        this.pass    = "123 ";
    }
    
    @Override
    public void conectar(){
        System.out.println("Conexión a MYSQL exitosa");
    }

    @Override
    public void desconectar(){
        System.out.println("Cerrando conexión a MYSQL");
    } 

    @Override
    public String toString() {
        return "ConnectionMySQL{" + "host=" + host + ", puerto=" + puerto + ", usuario=" + usuario + ", pass=" + pass + '}';
    }
    
    
    
}
