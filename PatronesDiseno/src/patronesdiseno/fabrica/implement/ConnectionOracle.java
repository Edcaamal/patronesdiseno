/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesdiseno.fabrica.implement;

import patronesdiseno.fabrica.IConnectionFabrica;

/**
 *
 * @author edgar
 */
public class ConnectionOracle implements IConnectionFabrica{
    
    private String host;
    private String puerto;
    private String usuario;
    private String pass;

    public ConnectionOracle() {
        this.host    = "localhost";
        this.puerto  = "1521";
        this.usuario = "admin";
        this.pass    = "123 ";

    }

    @Override
    public void conectar() {
        System.out.println("Conexión a Oracle exitosa");
     }

    @Override
    public void desconectar() {
        System.out.println("Cerrando conexión a Oracle");
     }

    
    
    @Override
    public String toString() {
        return "ConnectionOracle{" + "host=" + host + ", puerto=" + puerto + ", usuario=" + usuario + ", pass=" + pass + '}';
    }


    
    
    
}
